import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button, StatusBar } from 'react-native';

export default function App() {
  const [todos, setTodos] = useState([]);
  const [info, setInfo] = useState('');

  useEffect(() => {
    getTodos();
  }, []);

  const getTodos = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setTodos(data);
    } catch (error) {
      console.error('Error al obtener los pendientes:', error);
      setTodos([]);
    }
  };

  const showInformation = (option) => {
    switch (option) {
      case '1':
        const ids = todos.map(todo => todo.id).join(', ');
        setInfo(`ID de todos los pendientes: ${ids}`);
        break;
      case '2':
        const idsWithTitle = todos.map(todo => `ID: ${todo.id}, Título: ${todo.title}`).join('\n');
        setInfo(`IDs con titulo de los pendientes:\n${idsWithTitle}`);
        break;
      case '3':
        const pendingTodos = todos.filter(todo => !todo.completed);
        const pendingTitles = pendingTodos.map(todo => `ID: ${todo.id}, Título: ${todo.title}`).join('\n');
        setInfo(`Pendientes no resueltos:\n${pendingTitles}`);
        break;
      case '4':
        const completedTodos = todos.filter(todo => todo.completed);
        const completedTitles = completedTodos.map(todo => `ID: ${todo.id}, Título: ${todo.title}`).join('\n');
        setInfo(`Pendientes resueltos:\n${completedTitles}`);
        break;
      case '5':
        const todosWithIdsAndUserId = todos.map(todo => `ID: ${todo.id}, User: ${todo.userId}`).join('\n');
        setInfo(`Pendientes con IDs y userID:\n${todosWithIdsAndUserId}`);
        break;
      case '6':
        const completedTodosWithUserId = todos.filter(todo => todo.completed).map(todo => `ID: ${todo.id}, UserID: ${todo.userId}`).join('\n');
        setInfo(`Pendientes resueltos con ID y userID:\n${completedTodosWithUserId}`);
        break;
      case '7':
        const pendingTodosWithUserId = todos.filter(todo => !todo.completed).map(todo => `ID: ${todo.id}, UserID: ${todo.userId}`).join('\n');
        setInfo(`Pendientes no resueltos con ID y UserID:\n${pendingTodosWithUserId}`);
        break;
      default:
        setInfo('Opción inválida');
    }
  };

  return (
    <View style={styles.container}>
      <Text>Seleccione una opción:</Text>
      <Button title="ID de todos los pendientes" onPress={() => showInformation('1')} />
      <Button title="IDs con titulo de los pendientes" onPress={() => showInformation('2')} />
      <Button title="Pendientes no resueltos" onPress={() => showInformation('3')} />
      <Button title="Pendientes resueltos" onPress={() => showInformation('4')} />
      <Button title="Pendientes con IDs y userID" onPress={() => showInformation('5')} />
      <Button title="pendientes resueltos con ID y userID" onPress={() => showInformation('6')} />
      <Button title="Pendientes no resueltos con ID y UserID" onPress={() => showInformation('7')} />
      <Text style={styles.info}>{info}</Text>
      <StatusBar hidden={false} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  info: {
    marginVertical: 20,
    textAlign: 'center',
  },
});
