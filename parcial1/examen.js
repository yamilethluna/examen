

const { todo } = require('node:test');
async function getTodos() {
    try {
        const response = await fetch('http://jsonplaceholder.typicode.com/todos');
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error al obtener los pendientes:', error);
        return [];
    }
}

async function showInformation(option) {
    try {
        const todos = await getTodos();
        switch (option) {
            case 1:
                console.log('ID de todos los pendientes:');
                todos.forEach(todo => console.log(todo.id));
                break;
            case 2:
                console.log('IDs con titulo de los pendientes:');
                todos.forEach(todo => console.log(`ID: ${todo.id}, Título: ${todo.title}`));
                break;
            case 3:
                console.log('Pendientes no resueltos:');
                todos.filter(todo => !todo.completed).forEach(todo => console.log(`ID: ${todo.id}, Título: ${todo.title}`));
                break;
            case 4:
                console.log('Pendientes resueltos:');
                todos.filter(todo => todo.completed).forEach(todo => console.log(`ID: ${todo.id}, Título: ${todo.title}`));
                break;
            case 5:
                console.log('Pendientes con IDs y userID:');
                todos.forEach(todo => console.log(`ID: ${todo.id}, User: ${todo.userId}`));
                break;
            case 6:
                console.log('pendientes resueltos con ID y userID:');
                todos.filter(todo => todo.completed).forEach(todo => console.log(`ID: ${todo.id}, UserID: ${todo.userId}`));
                break;
            case 7:
                console.log('Pendientes no resueltos con ID y UserID:');
                todos.filter(todo => !todo.completed).forEach(todo => console.log(`ID: ${todo.id}, UserID: ${todo.userId}`));
                break;
            default:
                console.log('Opción inválida');
        }
    } catch (error) {
        console.error('Error al mostrar información:', error);
    }
}

// Menú
function showMenu() {
    console.log('1. Lista de todos los pendientes solo con ID');
    console.log('2. Lista de todos los pendientes con ID y titulo');
    console.log('3. Lista de todos los pendientes sin resolver con ID y titulo');
    console.log('4. Lista de todos los pendientes resueltos con ID y title');
    console.log('5. Lista de todos los pendientes con IDs y userId');
    console.log('6. Lista de todos los pendientes resueltos con IDs y userID');
    console.log('7. Lista de todos los pendientes con IDs y userID');
}showMenu();

//Leer la opcion que seleccione el usuario
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

readline.question('Ingrese una opción: ', option => {
    showInformation(parseInt(option));
    readline.close();
});
